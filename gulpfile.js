var devBase = './src/';
var devSecondaryBase = '/src/';
var prodBase = './dist/';
var prodSecondaryBase = '/dist/';
var appName = 'BaseTemplateApp';

// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var clean = require('gulp-clean');
var html2js = require('gulp-ng-html2js');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCSS = require('gulp-minify-css');
var bowerFiles = require('main-bower-files');
var inject = require('gulp-inject');
var underscore = require('underscore');
var underscoreStr = require('underscore.string');
var angularFilesort = require('gulp-angular-filesort');
var jshint = require('gulp-jshint');
var runSequence = require('run-sequence');
var webserver = require('gulp-webserver');
var gulpFilter = require('gulp-filter');

// Lint Task
gulp.task('lint', function() {
    return gulp.src([devBase + 'app/*.js', devBase + 'app/**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('clean', function() {
	return gulp.src([prodBase + '*'], {read:false})
		.pipe(clean({force: true}));
});

gulp.task('html2js', function() {
	return gulp.src([devBase + 'app/**/*.html'])
		.pipe(minifyHtml({
			empty: true,
			spare: true,
			quotes: true
		}))
		.pipe(html2js({
			moduleName: appName,
			prefix: '/app/'
		}))
		.pipe(concat('partials.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(devBase + 'app')); // move it to the root of the dev
});

gulp.task('bowerComponentsCSS', function() {
	var cssFiles = [];
	var allBowerFiles = bowerFiles();
	underscore.each(allBowerFiles, function(file){
	  if(underscoreStr.endsWith(file, '.css')){
		cssFiles.push(file);
	  }
	});
	
	return gulp.src(cssFiles)
        .pipe(concat('components.min.css'))
		.pipe(minifyCSS())
        .pipe(gulp.dest(prodBase + 'css/'));
});

gulp.task('bowerComponentsJS', function() {
	var jsFiles = [];
	var allBowerFiles = bowerFiles();
	underscore.each(allBowerFiles, function(file){
	  if(underscoreStr.endsWith(file, '.js')){
		jsFiles.push(file);
	  }
	});
	
	return gulp.src(jsFiles)
        .pipe(concat('components.min.js'))
		.pipe(uglify())
        .pipe(gulp.dest(prodBase + 'scripts/'));
});

gulp.task('copy', function() {
	gulp.src([devBase + 'authComplete.html',devBase + 'connectComplete.html'], {
			base: devBase
		}).pipe(gulp.dest(prodBase));	
	
	gulp.src(devBase + 'scripts/*.js', {
			base: devBase
		})
		.pipe(uglify())
		.pipe(gulp.dest(prodBase));
	
	gulp.src(devBase + 'content/css/*.css')
		.pipe(concat('site.min.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest(prodBase + 'css'));
		
	/* we need all the bower files that aren't js or css copied over as well */
	var fontFilter = gulpFilter(['*.otf', '*.eot', '*.svg', '*.ttf', '*.woff', '*.woff2']);
	var allBowerFiles = bowerFiles();
	gulp.src(allBowerFiles)
		.pipe(fontFilter)
		.pipe(gulp.dest(prodBase + 'fonts'));
});

// Concatenate & Minify custom js
gulp.task('angularApp', function() {
    return gulp.src([devBase + 'app/*.js', devBase + 'app/**/*.js'])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(prodBase + 'scripts'));
});

gulp.task('indexInjectionDev', function() {
	var allFiles = bowerFiles();
	allFiles.push(devBase + 'content/css/*.css');

	return gulp.src(devBase + 'index.html')
		.pipe(inject(gulp.src(allFiles, {read: false}), 
		{
			transform: function (filepath, file, i, length) {
			  filepath = filepath.replace(devSecondaryBase, '');
			  if(filepath.indexOf('.css') > -1)
			  {
				  if(filepath.indexOf('bower') > -1)
				  {
					return '<link href="..' + filepath + '" rel="stylesheet" />';
				  }
				  else
				  {
					return '<link href="' + filepath + '" rel="stylesheet" />';
				  }
			  }
			  else if(filepath.indexOf('.js') > -1)
			  {
				  return '<script src="..' + filepath + '"></script>';
			  }
			}
		}))
		.pipe(inject(
			gulp.src([devBase + 'app/*.js', devBase + 'app/**/*.js', '!' + devBase + 'app/partials.min.js']).pipe(angularFilesort()),
			{
				name: 'angular',
				transform: function (filepath, file, i, length) {
					filepath = filepath.replace(devSecondaryBase, '');
					return '<script src="' + filepath + '"></script>';
				}
			}
		))
		.pipe(gulp.dest(devBase));
});

gulp.task('indexInjectionProd', function() {
	return gulp.src(devBase + 'index.html')
		.pipe(inject(gulp.src(prodBase + 'css/*.css', {read: false}), 
		{
			transform: function (filepath, file, i, length) {
			  return '<link href="' + filepath.replace(prodSecondaryBase, '') + '" rel="stylesheet" />'
			}
		}))
		.pipe(inject(gulp.src(prodBase + 'scripts/components.min.js', {read: false}), 
		{
			transform: function (filepath, file, i, length) {
			  return '<script src="' + filepath.replace(prodSecondaryBase, '') + '"></script>'
			}
		}))
		.pipe(inject(gulp.src([
			prodBase + 'scripts/*.js',
			'!' + prodBase + 'scripts/components.min.js',
			'!' + prodBase + 'scripts/authComplete.js',
			'!' + prodBase + 'scripts/connectComplete.js'
		], {read: false}),{
			name: 'angular',
			transform: function (filepath, file, i, length) {
			  return '<script src="' + filepath.replace(prodSecondaryBase, '') + '"></script>'
			}
		}))
		.pipe(gulp.dest(prodBase));
});

// Default Task
gulp.task('default');

gulp.task('server:prod', function(){
	runSequence(
		'clean',
		'html2js',
		[
			'copy',
			'angularApp',
			'bowerComponentsJS',
			'bowerComponentsCSS'
		],
		'indexInjectionProd',
		'createHostProd'
	);
});

gulp.task('server', function(){
	runSequence('indexInjectionDev', 'createHostDev');
});

gulp.task('createHostProd', function(){
	gulp.src([prodBase, '.'])
		.pipe(webserver({
		  host: 'localhost',
		  livereload: true,
		  https: 'true',
		  port: '32150',
		  open: true
		}));	
});

gulp.task('createHostDev', function(){
	gulp.src([devBase, '.'])
		.pipe(webserver({
		  host: 'localhost',
		  livereload: true,
		  https: 'true',
		  port: '32150',
		  open: true
		}));	
});























