﻿(function() {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngStorage',
        'landing',
        'account'
    ]);

})();