(function() {
    'use strict';

    angular.module('app')
        .directive('navbar', navbarDirective);

    navbarDirective.$inject = [];

    function navbarDirective() {

        return {
            restrict: 'E',
            controller: 'NavbarController',
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'app/common/directives/partials/navbar.html',
            replace: true,
            scope: true
        };
    }
})();