﻿(function () {
    'use strict';

    angular.module('app')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$location', 'accountService'];

    function NavbarController($location, accountService) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.logOut = logOut;

        // scope variables
        vm.authentication = accountService.getAuthenticationData();

        /////////////////////////////////////////////////////////

        function logOut() {
            accountService.logOut();
            $location.path('/home');
        }
    }

})();