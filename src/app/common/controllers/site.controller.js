(function() {
    'use strict';

    angular.module('app')
        .controller('SiteController', SiteController);

    SiteController.$inject = ['$injector'];

    function SiteController($injector) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.callbackAction = callbackAction;

        /////////////////////////////////////////////////////////

        function callbackAction(callbackMethod, callbackService, callbackData) {
            var service = $injector.get(callbackService);

            service[callbackMethod](callbackData).then(function(response){

            });
        }
    }

})();