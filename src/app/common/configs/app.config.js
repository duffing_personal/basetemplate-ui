﻿(function () {
	'use strict';

	angular.module('app')
        .config(AppConfig);

	AppConfig.$inject = ['$httpProvider', '$urlRouterProvider'];

	function AppConfig($httpProvider, $urlRouterProvider) {
		$httpProvider.interceptors.push('authInterceptorService');

        $urlRouterProvider
            .when('/', '/landing')
            .when('', '/landing')
            .otherwise('/landing');
    }

})();
