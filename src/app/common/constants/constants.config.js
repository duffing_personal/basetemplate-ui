﻿(function () {
   'use strict';
   
	angular.module('app')
		.constant('ngAuthSettings', {
			apiAuthenticationBaseUri: 'https://localhost:26264/auth/',
			apiServiceBaseUri: 'https://localhost:47039/api/',
			clientId: 'ngAuthApp'
		});

})();