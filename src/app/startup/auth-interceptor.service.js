﻿(function () {
    'use strict';

    angular.module('app')
        .service('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', '$injector', '$location', '$localStorage', '$rootScope'];

    function authInterceptorService($q, $injector, $location, $localStorage, $rootScope) {
        var $http;

        var service = {
            request: request,
            response: response,
            responseError: responseError
        };

        return service;

        ////////////////////////////////

        function retryHttpRequest(config, deferred) {
            $http = $http ||  $injector.get('$http');

            $http(config).then(function (response) {
                    deferred.resolve(response);
                },
                function (response) {
                    deferred.reject(response);
                });
        }

        function request(config) {
            config.headers = config.headers || {};

            var authData = $localStorage.authorizationData;

            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        function response(response) {
            return response;
        }

        function responseError(rejection) {
            var deferred = $q.defer();
            if (rejection.status === 401) {
                var loginData = $injector.get('loginData');
                var accountService = $injector.get('accountService');

                loginData.refreshToken().then(function(response) {
                        retryHttpRequest(rejection.config, deferred);
                    },
                    function() {
                        accountService.logOut();
                        $location.path('/login');
                        deferred.reject(rejection);
                    });
            }
            else {
                if(rejection.headers('callbackService' !== null) || (rejection.data !== null && rejection.data.modelState === undefined)) { // we need to show the notifications up top
                    var callbackUrl = (rejection.headers('callbackUrl') === undefined || rejection.headers('callbackUrl') === null) ? '' : rejection.headers('callbackUrl');
                    var callbackMethod = rejection.headers('callbackMethod');
                    var callbackService = rejection.headers('callbackService');
                    var callbackText = rejection.headers('callbackText');
                    var callbackData = rejection.headers('callbackData');

                    $rootScope.notifications.push({
                        'message': rejection.data.error_description,
                        'callbackUrl': callbackUrl,
                        'callbackText': callbackText,
                        'callbackMethod': callbackMethod,
                        'callbackService': callbackService,
                        'callbackData': callbackData
                    });
                }

                deferred.reject(rejection);
            }
            return deferred.promise;
        }
    }

})();