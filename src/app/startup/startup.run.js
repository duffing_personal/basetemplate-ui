(function () {
    'use strict';

    angular.module('app')
        .run(RunApp);

    RunApp.$inject = ['accountService', '$rootScope'];

    function RunApp(accountService, $rootScope) {
        $rootScope.notifications = [];
        accountService.fillAuthData();
    }

})();