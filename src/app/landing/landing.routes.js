(function () {
    'use strict';

    angular.module('landing')
        .config(landingRoutesConfig);

    landingRoutesConfig.$inject = ['$stateProvider'];

    function landingRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('landing', {
                url: '/landing',
                templateUrl: '/app/landing/views/landing.html',
                controller: 'LandingController',
                controllerAs: 'vm'
            })
    }

})();