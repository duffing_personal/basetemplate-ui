(function () {
    'use strict';

    angular.module('forgot-password')
        .config(forgotPasswordRoutesConfig);

    forgotPasswordRoutesConfig.$inject = ['$stateProvider'];

    function forgotPasswordRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('forgotPassword', {
                url: '/forgotPassword',
                templateUrl: '/app/account/forgot-password/views/forgot-password.html',
                controller: 'ForgotPasswordController',
                controllerAs: 'vm'
            });
    }

})();