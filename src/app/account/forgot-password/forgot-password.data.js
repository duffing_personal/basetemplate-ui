﻿(function () {
    'use strict';

    angular.module('forgot-password-data', [])
        .service('forgotPasswordData', forgotPasswordData);

    forgotPasswordData.$inject = ['$http', 'ngAuthSettings'];

    function forgotPasswordData($http, ngAuthSettings) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            sendPasswordEmail: sendPasswordEmail
        };

        return service;

        ///////////////////////////////////////////

        function sendPasswordEmail(userName) {
            return $http.get(authenticationBase + 'Authorization/PasswordResetEmail', { params: { userName: userName } }).success(function(response) {
                return response;
            });
        }
    }

})();