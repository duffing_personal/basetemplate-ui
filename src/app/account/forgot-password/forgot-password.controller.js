﻿(function() {
    'use strict';

    angular.module('forgot-password-controller', [])
        .controller('ForgotPasswordController', ForgotPasswordController);

    ForgotPasswordController.$inject = ['forgotPasswordData', '$timeout', '$location'];

    function ForgotPasswordController(forgotPasswordData, $timeout, $location) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.sendPasswordEmail = sendPasswordEmail;

        // scope variables
        vm.message = '';
        vm.sentSuccessfully = false;
        vm.userName = '';

        /////////////////////////////////////////////////////////

        function beginTimer() {
            var timer = $timeout(function() {
                $timeout.cancel(timer);
                $location.path('/login');
            }, 2000);
        }

        function sendPasswordEmail(userName) {
            forgotPasswordData.sendPasswordEmail(userName).then(function(response) {
                vm.sentSuccessfully = true;
                vm.message = 'Password reset email has been sent successfully, you will be redirected to the login page in 2 seconds.';
                beginTimer();
            },
            function(response) {
                if(response.data.modelState !== undefined) {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                    vm.message = 'Failed to send password reset email: ' + errors.join(' ');
                }
            });
        }
    }

})();