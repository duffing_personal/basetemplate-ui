(function() {
    'use strict';

    angular.module('forgot-password', [
        'forgot-password-controller',
        'forgot-password-data'
    ]);

})();