(function () {
    'use strict';

    angular.module('reset-password')
        .config(resetPasswordRoutesConfig);

    resetPasswordRoutesConfig.$inject = ['$stateProvider'];

    function resetPasswordRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('resetPassword', {
                url: '/resetPassword?/userId/token',
                templateUrl: '/app/account/reset-password/views/reset-password.html',
                controller: 'ResetPasswordController',
                controllerAs: 'vm'
            });
    }

})();