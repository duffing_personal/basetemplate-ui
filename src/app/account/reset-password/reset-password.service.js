﻿(function() {
    'use strict';

    angular.module('reset-password-data', [])
        .service('resetPasswordData', resetPasswordData);

    resetPasswordData.$inject = ['$http', 'ngAuthSettings'];

    function resetPasswordData($http, ngAuthSettings) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            resetPassword: resetPassword
        };

        return service;

        //////////////////////////////////////////////////

        function resetPassword(userId, password, confirmPassword, token) {
            var resetModel = {
                userId: userId,
                confirmPassword: confirmPassword,
                password: password,
                token: token
            };

            return $http.post(authenticationBase + 'Authorization/ResetPassword', resetModel).then(function(response) {
                return response;
            });
        }
    }

})();