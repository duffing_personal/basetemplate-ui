(function() {
    'use strict';

    angular.module('reset-password', [
        'reset-password-controller',
        'reset-password-data'
    ]);

})();