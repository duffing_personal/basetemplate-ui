﻿(function () {
    'use strict';

    angular.module('reset-password-controller', [])
        .controller('ResetPasswordController', ResetPasswordController);

    ResetPasswordController.$inject = ['resetPasswordData', '$stateParams', '$location', '$timeout'];

    function ResetPasswordController(resetPasswordData, $stateParams, $location, $timeout) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.resetPassword = resetPassword;

        // scope variables
        vm.message = '';
        vm.savedSuccessfully = false;

        /////////////////////////////////////////////////////////

        function beginTimer() {
            var timer = $timeout(function() {
                $timeout.cancel(timer);
                $location.path('/login');
            }, 2000);
        }

        function resetPassword(passwordModel) {
            var userId = $stateParams.userId;
            var token = $stateParams.token;

            resetPasswordData.resetPassword(userId, passwordModel.password, passwordModel.confirmPassword, token).then(function(response) {
                vm.savedSuccessfully = true;
                vm.message = 'Password has been reset successfully, you will be redirected to login page in 2 seconds.';
                beginTimer();
            },
            function(response) {
                if(response.data.modelState !== undefined) {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                    vm.message = 'Failed to reset password due to: ' + errors.join(' ');
                }
            });
        }
    }

})();