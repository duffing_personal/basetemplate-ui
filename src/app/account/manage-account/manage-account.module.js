(function() {
    'use strict';

    angular.module('manage-account', [
        'manage-account-controller',
        'manage-account-data'
    ]);

})();