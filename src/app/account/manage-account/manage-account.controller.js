﻿(function() {
    'use strict';

    angular.module('manage-account-controller', [])
        .controller('ManageAccountController', ManageAccountController);

    ManageAccountController.$inject = ['manageAccountData', 'ngAuthSettings', '$localStorage', '$timeout', '$scope', 'manageInfo'];

    function ManageAccountController(manageAccountData, ngAuthSettings, $localStorage, $timeout, $scope, manageInfo) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.linkLogin = linkLogin;
        vm.linkLoginComplete = linkLoginComplete;
        vm.removeLogin = removeLogin;
        vm.setPassword = setPassword;

        // scope variables
        vm.createPassword = {};
        vm.savedSuccessfully = false;
        vm.manageInfo = manageInfo;
        vm.message = '';

        /////////////////////////////////////////////////////////

        init();

        function beginTimer() {
            var timer = $timeout(function() {
                $timeout.cancel(timer);
                vm.manageInfo.hasPassword = true;
            }, 2000);
        }

        function init() {
            vm.createPassword = {
                password: '',
                confirmPassword: ''
            };
        }

        function linkLogin(provider) {
            var authData = $localStorage.authorizationData;

            var externalProviderUrl = ngAuthSettings.apiAuthenticationBaseUri + 'Manage/LinkLogin?userName=' + authData.userName + '&provider=' + provider + '&hashedSecurityToken=' + vm.manageInfo.hashedSecurityToken;

            window.$windowScope = vm;

            window.open(externalProviderUrl, 'Authenticate Account', 'location=0,status=0,width=600,height=750');
        }

        function linkLoginComplete() {
            $scope.$apply(function() {
                manageAccountData.getManageInfo().then(function(response) {
                    vm.manageInfo = response.data;
                });
            });
        }

        function removeLogin(login) {
            manageAccountData.removeLogin(login).then(function(response) {
                if (response.status === 200) {
                    manageAccountData.getManageInfo().then(function(response) {
                        vm.manageInfo = response.data;
                    });
                }
            });
        }

        function setPassword() {
            manageAccountData.setPassword(vm.createPassword).then(function(response) {
                vm.savedSuccessfully = true;
                vm.message = 'Your password has been set.';
                beginTimer();
            });
        }
    }

})();