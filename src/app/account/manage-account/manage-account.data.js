﻿(function () {
    'use strict';

    angular.module('manage-account-data', [])
        .service('manageAccountData', manageAccountData);

    manageAccountData.$inject = ['$http', 'ngAuthSettings'];


    function manageAccountData($http, ngAuthSettings) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            getManageInfo: getManageInfo,
            removeLogin: removeLogin,
            setPassword: setPassword
        };

        return service;

        ///////////////////////////////////////////////////

        function getManageInfo() {
            return $http.get(authenticationBase + 'Manage/ManageInfo').success(function(response) {
                return response;
            });
        }

        function removeLogin(login) {
            return $http.post(authenticationBase + 'Manage/RemoveLogin', login).then(function(response) {
                return response;
            });
        }

        function setPassword(passwordInfo) {
            return $http.post(authenticationBase + 'Manage/CreatePassword', passwordInfo).then(function(response) {
                return response;
            });
        }
    }

})();