(function () {
    'use strict';

    angular.module('manage-account')
        .config(manageAccountRoutesConfig);

    manageAccountRoutesConfig.$inject = ['$stateProvider'];

    function manageAccountRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('manage', {
                url: '/manage',
                templateUrl: '/app/account/manage-account/views/manage-account.html',
                controller: 'ManageAccountController',
                controllerAs: 'vm',
                resolve: {
                    manageInfo: ['manageAccountData', function(manageAccountData) {
                        return manageAccountData.getManageInfo().then(function (response) {
                            return response.data;
                        });
                    }]
                }
            });
    }

})();