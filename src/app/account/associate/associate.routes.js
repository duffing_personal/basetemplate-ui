(function () {
    'use strict';

    angular.module('associate')
        .config(associateRoutesConfig);

    associateRoutesConfig.$inject = ['$stateProvider'];

    function associateRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('associate', {
                url: '/associate',
                templateUrl: '/app/account/associate/views/associate.html',
                controller: 'AssociateController',
                controllerAs: 'vm'
            });
    }

})();