(function() {
    'use strict';

    angular.module('associate', [
        'associate-controller',
        'associate-data'
    ]);

})();