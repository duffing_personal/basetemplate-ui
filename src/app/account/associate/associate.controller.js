﻿(function() {
    'use strict';

    angular.module('associate-controller', [])
        .controller('AssociateController', AssociateController);

    AssociateController.$inject = ['$location', '$timeout', 'associateData', 'accountService'];

    function AssociateController($location, $timeout, associateData, accountService) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.registerExternal = registerExternal;

        // scope variables
        vm.email = ''; // used for showing the field or not
        vm.message = '';
        vm.registerData = {};
        vm.savedSuccessfully = false;

        /////////////////////////////////////////////////////////

        init();

        function beginTimer() {
            var timer = $timeout(function() {
                $timeout.cancel(timer);
                $location.path('/home');
            }, 5000);
        }

        function init() {
            var externalAuthData = accountService.getExternalAuthData();

            vm.email = externalAuthData.email;

            vm.registerData = {
                userName: externalAuthData.userName,
                provider: externalAuthData.provider,
                externalAccessToken: externalAuthData.externalAccessToken,
                email: externalAuthData.email
            };
        }

        function registerExternal() {
            associateData.registerExternal(vm.registerData).then(function(response) {
                vm.savedSuccessfully = true;
                if (response.data.length > 0) {
                    vm.message = 'User has been registered successfully, you will be redirected to homepage in 5 seconds.';
                }
                else {
                    vm.message = 'User registered successfully, but you still need to confirm your email. Please check your email for confirmation link.';
                }

                beginTimer();
            },
            function(response) {
                if(response.data.modelState !== undefined) {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        errors.push(response.data.modelState[key]);
                    }
                    vm.message = 'Failed to register user due to:' + errors.join(' ');
                }
            });
        }
    }

})();