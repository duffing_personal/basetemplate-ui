(function() {
    'use strict';

    angular.module('associate-data', [])
        .factory('associateData', associateData);

    associateData.$inject = ['$http', '$localStorage', 'accountService'];

    function associateData($http, $localStorage, accountService) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            registerExternal: registerExternal
        };

        return service;

        /////////////////////////////////////////////////////////

        function registerExternal(registerExternalData) {
            return $http.post(authenticationBase + 'Authorization/registerexternal', registerExternalData).success(function (response) {

                if (response.length > 0) {

                    $localStorage.authorizationData = {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: '',
                        useRefreshTokens: false,
                        email: response.email
                    };

                    var authData = {
                        isAuth: true,
                        userName: response.userName,
                        useRefreshTokens: false,
                        email: response.email
                    };

                    accountService.setAuthenticationData(authData);
                }

                return response;

            }).error(function (err, status) {
                accountService.logOut();

                return err;
            });
        }
    }

})();