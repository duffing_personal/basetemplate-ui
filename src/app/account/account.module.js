(function() {
    'use strict';

    angular.module('account', [
        'account-service',
        'associate',
        'forgot-password',
        'login',
        'manage-account',
        'reset-password',
        'signup'
    ]);

})();