﻿(function() {
    'use strict';

    angular.module('login-controller', [])
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'loginData', 'accountService', 'ngAuthSettings', '$scope'];

    function LoginController($location, loginData, accountService, ngAuthSettings, $scope) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.authCompletedCB = authCompletedCB;
        vm.authExternalProvider = authExternalProvider;
        vm.login = login;

        // scope variables
        vm.loginInfo = {};
        vm.message = '';

        /////////////////////////////////////////////////////////

        init();

        function authCompletedCB(fragment) {
            $scope.$apply(function() {
                if (fragment.haslocalaccount == 'False') {
                    accountService.logOut();

                    accountService.setExternalAuthData({
                        provider: fragment.provider,
                        userName: fragment.external_user_name,
                        externalAccessToken: fragment.external_access_token,
                        email: fragment.external_email
                    });

                    $location.path('/associate');
                }
                else {
                    //Obtain access token and redirect to orders
                    var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };

                    loginData.obtainAccessToken(externalData).then(function(response) {
                        $location.path('/home');
                    });
                }
            });
        }

        function authExternalProvider(provider) {
            var redirectUri = 'https:' + '//' + location.host + '/auth-complete.html';

            var externalProviderUrl = ngAuthSettings.apiAuthenticationBaseUri + 'Authorization/RetrieveLoginExternal?provider=' + provider
                + '&response_type=token&client_id=' + ngAuthSettings.clientId
                + '&redirect_uri=' + redirectUri;

            window.$windowScope = vm;
            window.open(externalProviderUrl, 'Authenticate Account', 'location=0,status=0,width=600,height=750');
        }

        function init() {
            vm.loginInfo = {
                userName: '',
                password: '',
                useRefreshTokens: false
            };
        }

        function login() {
            loginData.login(vm.loginInfo).then(function(response) {
                $location.path('/home');
            });
        }
    }

})();