(function() {
    'use strict';

    angular.module('login', [
        'login-controller',
        'login-data'
    ]);

})();