(function() {
    'use strict';

    angular.module('login-data', [])
        .factory('loginData', loginData);

    loginData.$inject = ['$http', 'ngAuthSettings', 'accountService', '$localStorage'];

    function loginData($http, ngAuthSettings, accountService, $localStorage) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            login: login,
            obtainAccessToken: obtainAccessToken,
            refreshToken: refreshToken
        };

        return service;

        ///////////////////////////////////////////

        function login(loginInfo) {
            var data = 'grant_type=password&username=' + loginInfo.userName + '&password=' + loginInfo.password + '&client_id=' + ngAuthSettings.clientId;

            return $http.post(authenticationBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                $localStorage.authorizationData = {
                    token: response.access_token,
                    userName: loginInfo.userName,
                    refreshToken: response.refresh_token,
                    useRefreshTokens: true,
                    expirationTime: new Date((new Date()).getTime() + response.expires_in * 1000)
                };

                var authData = {
                    isAuth: true,
                    userName: loginInfo.userName,
                    useRefreshTokens: true,
                    email: ''
                };

                accountService.setAuthenticationData(authData);

                return response;

            }).error(function (err, status) {
                accountService.logOut();

                return err;
            });
        }

        function obtainAccessToken(externalData) {
            return $http.get(authenticationBase + 'Authorization/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

                $localStorage.authorizationData = {
                    token: response.access_token,
                    userName: response.userName,
                    refreshToken: '',
                    useRefreshTokens: false,
                    email: response.email
                };

                var authData = {
                    isAuth: true,
                    userName: response.userName,
                    useRefreshTokens: false,
                    email: response.email
                };

                accountService.setAuthenticationData(authData);

                return response;

            }).error(function (err, status) {
                accountService.logOut();

                return err;
            });
        }

        function refreshToken() {
            var authData = $localStorage.authorizationData;

            if (authData) {
                var data = 'grant_type=refresh_token&refresh_token=' + authData.refreshToken + '&client_id=' + ngAuthSettings.clientId;

                return $http.post(authenticationBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                    /*
                     * only remove the current refresh token if we've received a response. this prevents bugs if the server times out and we've removed the
                     * refresh token already (this would cause the user to be logged out). By only replacing the local refresh_token if a new one is received
                     * it is more consistent with the spec, which says sending a refresh_token is optional, but if the server does, then the client MUST
                     * replace its local value
                     */
                    delete $localStorage.authorizationData;

                    $localStorage.authorizationData = {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: response.refresh_token,
                        useRefreshTokens: true,
                        expirationTime: new Date((new Date()).getTime() + response.expires_in * 1000)
                    };

                    return response;

                }).error(function (err, status) {
                    accountService.logOut();

                    return err;
                });
            }
        }
    }
})();