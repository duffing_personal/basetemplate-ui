(function () {
    'use strict';

    angular.module('login')
        .config(loginRoutesConfig);

    loginRoutesConfig.$inject = ['$stateProvider'];

    function loginRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: '/app/account/login/views/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            });
    }

})();