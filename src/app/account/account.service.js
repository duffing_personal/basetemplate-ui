﻿(function () {
    'use strict';

    angular.module('account-service', [])
        .service('accountService', accountService);

    accountService.$inject = ['$localStorage'];

    function accountService($localStorage) {

        var authentication = {
            isAuth: false,
            userName: '',
            useRefreshTokens: false,
            email: ''
        };
        var externalAuthData = {};

        var service = {
            fillAuthData: fillAuthData,
            getAuthenticationData: getAuthenticationData,
            getExternalAuthData: getExternalAuthData,
            logOut: logOut,
            setAuthenticationData: setAuthenticationData,
            setExternalAuthData: setExternalAuthData
        };

        return service;

        /////////////////////////////////////////////////////////

        function fillAuthData() {
            var authData = $localStorage.authorizationData;

            if (authData) {
                authentication.isAuth = true;
                authentication.userName = authData.userName;
                authentication.useRefreshTokens = authData.useRefreshTokens;
            }
        }

        function getAuthenticationData() {
            return authentication;
        }

        function getExternalAuthData() {
            return externalAuthData;
        }

        function logOut() {
            delete $localStorage.authorizationData;

            authentication.isAuth = false;
            authentication.userName = '';
            authentication.useRefreshTokens = false;
            authentication.email = '';
        }

        function setAuthenticationData(authData) {
            authentication = authData;
        }

        function setExternalAuthData(externalAuth) {
            externalAuthData = externalAuth;
        }
    }

})();