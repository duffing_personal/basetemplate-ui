(function() {
    'use strict';

    angular.module('signup-data', [])
        .factory('signupData', signupData);

    signupData.$inject = ['$http', 'ngAuthSettings', 'accountService'];

    function signupData($http, ngAuthSettings, accountService) {

        var authenticationBase = ngAuthSettings.apiAuthenticationBaseUri;

        var service = {
            saveRegistration: saveRegistration
        };

        return service;

        /////////////////////////////////////////////////////////

        function saveRegistration(registration) {
            accountService.logOut();

            return $http.post(authenticationBase + 'Authorization/register', registration).then(function (response) {
                return response;
            });
        }
    }

})();