(function () {
    'use strict';

    angular.module('signup')
        .config(signupRoutesConfig);

    signupRoutesConfig.$inject = ['$stateProvider'];

    function signupRoutesConfig($stateProvider) {

        /** Define state routes and the template. */
        $stateProvider
            .state('signup', {
                url: '/signup',
                templateUrl: '/app/account/signup/views/signup.html',
                controller: 'SignupController',
                controllerAs: 'vm'
            });
    }

})();