﻿(function () {
    'use strict';

    angular.module('signup-controller', [])
        .controller('SignupController', SignupController);

    SignupController.$inject = ['$location', '$timeout', 'signupData'];

    function SignupController($location, $timeout, signupData) {
        /* jshint validthis: true */
        var vm = this;

        // scope methods
        vm.signUp = signUp;

        // scope variables
        vm.message = '';
        vm.registration = {};
        vm.savedSuccessfully = false;

        /////////////////////////////////////////////////////////

        init();

        function beginTimer() {
            var timer = $timeout(function() {
                $timeout.cancel(timer);
                $location.path('/login');
            }, 2000);
        }

        function init() {
            vm.registration = {
                userName: '',
                password: '',
                confirmPassword: ''
            };
        }

        function signUp() {
            signupData.saveRegistration(vm.registration).then(function(response) {
                vm.savedSuccessfully = true;
                vm.message = 'User has been registered successfully, you will be redicted to login page in 2 seconds.';
                beginTimer();
            },
            function(response) {
                if(response.data.modelState !== undefined) {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                    vm.message = 'Failed to register user due to:' + errors.join(' ');
                }
            });
        }
    }

})();