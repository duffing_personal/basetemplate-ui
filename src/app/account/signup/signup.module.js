(function() {
    'use strict';

    angular.module('signup', [
        'signup-controller',
        'signup-data'
    ]);

})();